/*
  Q Light Controller
  showjockey.cpp

  Copyright (c) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QStringList>
#include <stdint.h>
#include <QString>
#include <QDebug>
#include <QFile>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// The ShowJockey USB interface API
#include "USBIO.h"

#include "qlcmacros.h"
#include "showjockey.h"
#include "showjockeydevice.h"

/*****************************************************************************
 * Initialization
 *****************************************************************************/

ShowJockey::~ShowJockey()
{
}

void ShowJockey::init()
{
	while (m_devices.isEmpty() == false)
		delete m_devices.takeFirst();

	DWORD count = USBIO_GetDeviceCount(SJVIDPID);
	for (DWORD i = 0; i < count; i++)
	{
		ShowJockeyDevice* dev = new ShowJockeyDevice(i, this);
		m_devices << dev;
	}
}

QString ShowJockey::name()
{
    return QString("ShowJockey");
}

int ShowJockey::capabilities() const
{
    return QLCIOPlugin::Output;
}

/*****************************************************************************
 * Outputs
 *****************************************************************************/

void ShowJockey::openOutput(quint32 output)
{
	if (output < (quint32) m_devices.size())
		m_devices[output]->open();
}

void ShowJockey::closeOutput(quint32 output)
{
	if (output < (quint32) m_devices.size())
		m_devices[output]->close();
}

QStringList ShowJockey::outputs()
{
    QStringList list;
	for (int i = 0; i < m_devices.size(); i++)
		list << QString("%1: %2").arg(i + 1).arg(m_devices[i]->name());
    return list;
}

QString ShowJockey::outputInfo(quint32 output)
{
    QString str;

    str += QString("<HTML>");
    str += QString("<HEAD>");
    str += QString("<TITLE>%1</TITLE>").arg(name());
    str += QString("</HEAD>");
    str += QString("<BODY>");

    if (output == QLCIOPlugin::invalidLine())
    {
        str += QString("<H3>%1</H3>").arg(name());
        str += QString("<P>");
        str += tr("This plugin provides DMX output support for the ShowJockey "
                  "interface using the DLL supplied with the product.");
        str += QString("</P>");
    }
    else if (output < (quint32) m_devices.size())
    {
        str += QString("<H3>%1</H3>").arg(m_devices[output]->name());
		str += QString("<B>Type:</B> %1").arg(m_devices[output]->type());
    }

    str += QString("</BODY>");
    str += QString("</HTML>");

    return str;
}

void ShowJockey::writeUniverse(quint32 output, const QByteArray& universe)
{
	if (output < (quint32) m_devices.size())
		m_devices[output]->writeUniverse(universe);
}

/*****************************************************************************
 * Configuration
 *****************************************************************************/

void ShowJockey::configure()
{
}

bool ShowJockey::canConfigure()
{
    return false;
}

/*****************************************************************************
 * Plugin export
 ****************************************************************************/

Q_EXPORT_PLUGIN2(ShowJockey, ShowJockey)
