/*
  Q Light Controller
  showjockeydevice.cpp

  Copyright (c) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <windows.h>
#include <QSettings>
#include <QDebug>

#include "USBIO.h"

#include "showjockeydevice.h"

#define SETTINGS_MODE "showjockey/mode/%1"

ShowJockeyDevice::ShowJockeyDevice(DWORD index, QObject* parent)
    : QObject(parent)
    , m_index(index)
    , m_handle(NULL)
    , m_type(0)
    , m_mode(TXMode)
{
    qDebug() << Q_FUNC_INFO;

    QSettings settings;
    QString key = QString(SETTINGS_MODE).arg(index);
    QVariant var = settings.value(key);
    if (var.isValid() == true)
        m_mode = (ShowJockeyDevice::Mode) var.toInt();
    else
        m_mode = ShowJockeyDevice::TXMode;
}

ShowJockeyDevice::~ShowJockeyDevice()
{
    qDebug() << Q_FUNC_INFO;

    if (isOpen() == true)
        close();

    QSettings settings;
    QString key = QString(SETTINGS_MODE).arg(m_index);
    settings.setValue(key, (int) mode());
}

void ShowJockeyDevice::open()
{
    qDebug() << Q_FUNC_INFO;

    if (isOpen() == false)
    {
        m_handle = USBIO_OpenDevice(m_index, SJVIDPID);
        qDebug() << "Opened ShowJockey device number:" << m_index << "HANDLE:" << (void*) m_handle;
        if (m_handle != NULL)
            m_type = USBIO_GetDeviceType(m_handle);
        else
            qWarning() << "Unable to open ShowJockey device #" << m_index;
    }
}

void ShowJockeyDevice::close()
{
    qDebug() << Q_FUNC_INFO;

    if (isOpen() == true)
    {
        USBIO_CloseDevice(m_handle);
        m_handle = NULL;
    }
}

bool ShowJockeyDevice::isOpen() const
{
    if (m_handle != NULL)
        return true;
    else
        return false;
}

QString ShowJockeyDevice::name() const
{
    return QString("ShowJockey (Type:%1)").arg(type());
}

uint ShowJockeyDevice::type() const
{
    return m_type;
}

void ShowJockeyDevice::setMode(ShowJockeyDevice::Mode mode)
{
    qDebug() << Q_FUNC_INFO;
    m_mode = mode;
}

ShowJockeyDevice::Mode ShowJockeyDevice::mode() const
{
    return m_mode;
}

void ShowJockeyDevice::writeUniverse(const QByteArray& universe)
{
    if (isOpen() == true)
    {
        USBIO_SendDmx(m_handle, (PUCHAR) universe.data(), (USHORT) universe.size());
    }
}
