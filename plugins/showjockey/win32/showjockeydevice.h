/*
  Q Light Controller
  showjockeydevice.h

  Copyright (c) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef SHOWJOCKEYDEVICE_H
#define SHOWJOCKEYDEVICE_H

#include <QObject>
#include <windows.h>

#define SJVIDPID PCHAR("vid_0483&pid_57fe")

class ShowJockeyDevice : public QObject
{
	Q_OBJECT
public:
	ShowJockeyDevice(DWORD index, QObject* parent = 0);
	virtual ~ShowJockeyDevice();

	enum Mode {
		TXMode,
		RXMode
	};

	void open();
	void close();
	bool isOpen() const;
	uint type() const;
	QString name() const;
	void writeUniverse(const QByteArray& universe);

	void setMode(ShowJockeyDevice::Mode mode);
	Mode mode() const;

private:
	const DWORD m_index;
	HANDLE m_handle;
	USHORT m_type;
	Mode m_mode;
};

#endif