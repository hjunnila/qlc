include (../../../variables.pri)

TEMPLATE = lib
LANGUAGE = C++
TARGET   = showjockey
DEFINES += QLC_EXPORT

SHOWJOCKEY_PATH = "C:/Showjockey API V1.0/DMX_Win_V01.00/Showjockey usb interface API"
INCLUDEPATH += $$SHOWJOCKEY_PATH
INCLUDEPATH += ../../interfaces
DEPENDPATH  += $$SHOWJOCKEY_PATH
CONFIG      += plugin
win32:debug:LIBS += -L$$SHOWJOCKEY_PATH/Debug -lusbio
win32:release:LIBS += -L$$SHOWJOCKEY_PATH/Release -lusbio

# Headers
HEADERS += showjockey.h showjockeydevice.h

# Sources
SOURCES += showjockey.cpp showjockeydevice.cpp

HEADERS += ../../interfaces/qlcioplugin.h

# Installation
target.path = $$INSTALLROOT/$$PLUGINDIR
INSTALLS   += target
